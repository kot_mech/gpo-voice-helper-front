import ProvideAuth from "./context/provideAuth";
import {library} from '@fortawesome/fontawesome-svg-core'
import {fab} from '@fortawesome/free-brands-svg-icons'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {far} from '@fortawesome/free-regular-svg-icons'
import {useState, lazy, Suspense, useEffect} from "react";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import {Spinner} from "react-bootstrap";

const PublicLayout = lazy(() => import('./layouts/public'));
const PrivateLayout = lazy(() => import('./layouts/private'));

library.add(fab, fas, far)

function App() {
    return (
        <div className="App">
            <ProvideAuth>
                <Router keyLength={12}>
                    <Suspense fallback={<Spinner animation="border"/>}>
                        <Routes>
                            <Route path="/admin/*" element={<PrivateLayout/>} />
                            <Route path="*" element={<PublicLayout/>} />
                        </Routes>
                    </Suspense>
                </Router>
            </ProvideAuth>
        </div>
    );
}

export default App;
