import {Card, Button, Alert, Spinner, Accordion, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {useAuth} from "../../context/provideAuth";
import {useNavigate, useParams} from "react-router-dom";
import {includes} from "../../tools/objectManager";
import PathsContent from "../../components/pathsContent";
import DictionaryContent from "../../components/dictionaryContent";

const Project = function (props) {
    const [project, setProject] = useState(null);
    const [paths, setPaths] = useState([]);
    let auth = useAuth();
    let navigate = useNavigate();
    let params = useParams();

    useEffect(() => {
        request(
            `/project/one?code=${params.code}`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProject(response.data)
            }
        )
    }, [auth.token])

    const handleDelete = () => {
        request(
            `/project/`,
            'DELETE',
            {code: params.code},
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                navigate(-1)
            }
        )
    }

    if (!project) return <div
        className={'d-flex flex-column align-items-center justify-content-center w-100 max-width-1200'}>
        <Spinner animation={'border'}/>
    </div>

    return <div className={'d-flex flex-column align-items-center justify-content-center w-100 max-width-1200'}>
        <Card body className={'d-block p-1 mt-4 mx-2'}>
            <Breadcrumb>
                <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
                <Breadcrumb.Item active>Проект "{project.name}"</Breadcrumb.Item>
            </Breadcrumb>
            <h3 className={'mb-2 mt-0'}>Проект "{project.name}"</h3>
            <Accordion defaultActiveKey="0">
                <Accordion.Item eventKey="0">
                    <Accordion.Header>Управление путями</Accordion.Header>
                    <Accordion.Body>
                        <PathsContent onUpdate={setPaths}/>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header>Управление словарем</Accordion.Header>
                    <Accordion.Body>
                        <DictionaryContent paths={paths}/>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                    <Accordion.Header>Управление проектом</Accordion.Header>
                    <Accordion.Body>
                        <div className={'d-flex flex-row align-items-center justify-content-between'}>
                            <Button variant="outline-primary"
                                    onClick={() => navigate('/admin/project/' + params.code + '/edit')}>
                                <FontAwesomeIcon icon="fa-solid fa-pen"/> Редактировать
                            </Button>
                            {includes(auth.user.roles, 'ROLE_ADMIN') && <Button variant="outline-danger"
                                                                                onClick={handleDelete}>
                                <FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>}
                        </div>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </Card>
    </div>;
}

export default Project;
