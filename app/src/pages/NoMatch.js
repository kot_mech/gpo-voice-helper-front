import {
    Link,
    useLocation
} from "react-router-dom";
import {connect} from "react-redux";
import {Card} from "react-bootstrap";

const NoMatch = function () {
    let location = useLocation();

    return (
        <div className={'d-flex align-items-center justify-content-center flex-column w-100 h-100'}>
            <Card body className={'d-flex flex-column lign-items-center justify-content-center p-3 mt-4'}>
                <h3>
                    404: Страницы <code>{location.pathname}</code> не существует
                </h3>
                <p className={'text-center mb-0'}>
                    <Link to={'/'} >Вернуться на главную</Link>
                </p>
            </Card>
        </div>
    );
}

export default connect((state /*, ownProps*/) => {
    return state;
}, dispatch => ({}))(NoMatch);
